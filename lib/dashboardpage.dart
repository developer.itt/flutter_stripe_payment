import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:http/http.dart' as http;
import 'package:stripe_payment/stripe_payment.dart';
import 'package:stripe_payment/stripe_payment.dart' as paymentMethodqq;

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  Map<String, dynamic>? paymentintentData;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Center(
            child: Container(
              child: ElevatedButton(
                onPressed: generateToken,
                child: const Text('Pay Now'),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> makepayment() async {
    print('called');
    final url = Uri.parse(
        'https://us-centrall-flutterdemo-c949a.cloudfunctions.net/stripePayment');

    final response =
        await http.get(url, headers: {'Content-Type': 'application/json'});

    print('response$response');
    paymentintentData = json.decode(response.body);
    print('response$paymentintentData');

    await Stripe.instance.initPaymentSheet(
        paymentSheetParameters: SetupPaymentSheetParameters(
            paymentIntentClientSecret: paymentintentData!['paymentIntent'],
            applePay: true,
            googlePay: true,
            style: ThemeMode.dark,
            merchantCountryCode: 'US',
            merchantDisplayName: 'RAJA'));
    setState(() {});

    displayPaymnetsheet();
  }

  Future<void> displayPaymnetsheet() async {
    try {
      await Stripe.instance.presentPaymentSheet(
          parameters: PresentPaymentSheetParameters(
              clientSecret: paymentintentData!['paymentIntent'],
              confirmPayment: true));

      setState(() {
        paymentintentData = null;
      });
    } catch (e) {
      print(e);
    }
  }

  Future<void> generateToken() async {
    print('called');
    StripePayment.paymentRequestWithCardForm(CardFormPaymentRequest())
        .catchError((e) {
      print('ERROR ${e.toString()}');
    }).then((paymentMethodqq.PaymentMethod paymentMethod) async {
      final CreditCard testCard = CreditCard(
          number: '4242 4242 4242 4242',
          expMonth: 02,
          expYear: 25);

      StripePayment.createTokenWithCard(testCard).then((token) {
        print(token.tokenId);
      });
    });
  }
}
